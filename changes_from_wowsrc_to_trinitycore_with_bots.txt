
TC_LOG_DEBUG(LOG_FILTER_TSCR, 
has changed to 
TC_LOG_DEBUG("scripts",

LOG_FILTER_GENERAL
has changed to 
"misc"

LOG_FILTER_MAPS
has changed to
"maps"

getSource
changes to
GetSource



voice->AI()->Talk(SAY_VOICE_2, playerGUID);

examples of the old DoScriptText for scriptdev2 see change log 
Author:		Gacko <gacko28@gmx.de>

Date:		3 years ago (12/7/2012 1:04:40 AM)

Commit hash:	8fbec4156b887690e6724bc11f9dda5546d32f39

Parent(s):	f9c0e3c91b

Core/DB: Creature text for 22 script files


Contained in branches: 4.3.4
Contained in tags: TDB335.58, TDB6.01, TDB6.00, TDB434.09, TDB335.57, TDB335.56, TDB335.55, TDB335.54, TDB335.53, TDB335.52, TDB335.51, TDB335.50

for more info do a git search for DoScriptText

from
DoScriptText(SAY_PATHALEON_CULATOR_IMAGE_1, pathaleon);
                
to
pathaleon->AI()->Talk(SAY_PATHALEON_CULATOR_IMAGE_1);

and
from
DoScriptText(SAY_COMMANDER_DAWNFORGE_2, me);
                
to
Talk(SAY_COMMANDER_DAWNFORGE_2);

from
SAY_COMMANDER_DAWNFORGE_1       = -1000128,

    
to
SAY_COMMANDER_DAWNFORGE_1       = 0,